# trikka-discussion

This place if for discussing the [Trikka](https://trikka.house/) platform, which enables "modular products
from open multi-use parts".

This place is not "officially" operated by Trikka. It is meant as a **test bed** to explore community interactions.

Please participate if you have an interest in Trikka. :smiley:

### "I am not a programmer, I have no clue how to use Git"

No worries, please! This place uses the `Issues` feature of GitLab, which can be used "stand-alone" without touching Git at all! If you are familiar with disussion forums, `Issues` should be fairly intuitive to use -- just stick to the basic functionality when you start out. If you feel like it, you can try more advanced stuff later on.

## how to participate

On the left sidebar/panel, click on `Issues`. You will see a list of discussion items:
- Comment on an existing one that is of interest to you by clicking on it. In the Issue, **scroll down** to the bottom of the page and use the text box. 
- create a new discussion item by creating a new issue. At the top right of the page click on Ǹew Issue`. Just fill in the title and use the text box, ignore the other stuff.

:bulb:
For a text box, use the `Preview` tab to see the final formatting of your post.

:question:
More information about the Issues functionality can be found in the [documentation](https://docs.gitlab.com/ee/user/project/issues/index.html).

## license

:construction:

***

